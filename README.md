# Application Budget

## Classes

L'application comporte 4 classes dont trois seront amenées à êtres enregistrées en base.

### Budget

 Cette classe définira le budget total de l'utilisateur (Le montant de son compte en banque par exemple), et son montant sera modifié à chaque ajout d'une opération

#### Méthodes de Budget

 Cette classe sera composé de getter et setter ainsi que d'une méthode publiuqe 'updateTotal' qui modifiera le total en fonction d'une opération.

### Operation

 Cette classe servira à manipuler les transactions de l'utilisateur, on pourra créer une instance de la classe grâce à un montant, une date et un opérateur (+ ou -).

#### Méthodes d'Opération

 getter et setter

### Category

 On pourra classer les opérations en utilisant la classe Category (exemple : famille, loyer, etc..).

#### Méthodes de Category

getter et setter

### Stats

 Classe utilisée pour la création de statistiques basés sur une liste d'opérations.

#### Méthodes de Stats

- createStats, prendra en paramètre une liste d'opérations afin de créer des statistiques basées sur ces données.
